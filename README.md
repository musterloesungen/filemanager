**Aufgabe: Ein Shell-Skript zur Dateiverwaltung und -analyse**

**Ziel**: Erstellen Sie ein Shell-Skript namens `file_manager.sh`, mit dem Benutzer Dateien in einem angegebenen Verzeichnis analysieren, duplizieren und archivieren können.

**Anforderungen**:

1. **Dateianalyse**:
    - Bei Aufruf mit der Option `-l` oder `--list` sollte das Skript alle Dateien des angegebenen Verzeichnisses auflisten und deren Größe in einem benutzerfreundlichen Format (z.B. KB, MB) anzeigen.
    - Bei Aufruf mit der Option `-s` oder `--stats` sollte das Skript eine Zusammenfassung des Verzeichnisses anzeigen, einschließlich der Gesamtzahl der Dateien, des gesamten Speicherplatzes, der durchschnittlichen Dateigröße und der Größe der größten und kleinsten Datei.

2. **Dateimanipulation**:
    - Bei Aufruf mit der Option `-d` oder `--duplicate` gefolgt von einem Dateinamen, sollte das Skript eine Kopie der angegebenen Datei im gleichen Verzeichnis erstellen (z.B. `file.txt` wird zu `file_copy.txt`).
    - Bei Aufruf mit der Option `-r` oder `--rename` gefolgt von zwei Dateinamen, sollte das Skript die erste Datei in den zweiten Dateinamen umbenennen.

3. **Archivierung**:
    - Bei Aufruf mit der Option `-a` oder `--archive`, sollte das Skript alle Dateien im angegebenen Verzeichnis in eine tar.gz-Datei packen. Der Name des Archivs sollte das aktuelle Datum enthalten (z.B. `backup_2023-09-26.tar.gz`).

4. **Hilfemenü**:
    - Bei Aufruf mit der Option `-h` oder `--help` sollte das Skript ein kurzes Hilfemenü mit einer Beschreibung aller verfügbaren Optionen anzeigen.

**Bonuspunkte**:

- Implementieren Sie eine Option `-f` oder `--find`, die einen Dateinamen oder Teil eines Dateinamens akzeptiert und alle Dateien im Verzeichnis anzeigt, die diesen Namen oder Teilnamen enthalten.
- Verwenden Sie die `trap`-Funktion, um sicherzustellen, dass das Skript bei einem unerwarteten Beenden (z.B. durch STRG+C) ordnungsgemäß beendet wird.
- Implementieren Sie Fehlerbehandlung für den Fall, dass Dateien oder Verzeichnisse nicht gefunden werden oder andere Probleme auftreten.
