#!/bin/bash

# Basisverzeichnis setzen
BASE_DIR=$(pwd)

# Dateianalyse - Auflisten
list_files() {
    OUTPUT=$(ls -lh "$BASE_DIR" | awk '{print $9 " " $5}')
    dialog --title "Dateiliste" --msgbox "$OUTPUT" 20 60
}

# Dateianalyse - Statistiken
show_stats() {
    TOTAL_FILES=$(ls -1q "$BASE_DIR" | wc -l)
    TOTAL_SIZE=$(du -sh "$BASE_DIR" | cut -f1)
    # Weitere Statistiken wie Durchschnittsgröße, größte/kleinste Datei können hier hinzugefügt werden
    OUTPUT="Gesamtzahl der Dateien: $TOTAL_FILES\nGesamte Verzeichnisgröße: $TOTAL_SIZE"
    dialog --title "Statistiken" --msgbox "$OUTPUT" 20 60
}

# Datei duplizieren
duplicate_file() {
    FILENAME=$(dialog --title "Datei duplizieren" --inputbox "Geben Sie den Namen der Datei ein, die Sie duplizieren möchten:" 10 60 3>&1 1>&2 2>&3)
    cp "$BASE_DIR/$FILENAME" "$BASE_DIR/${FILENAME}_copy"
    dialog --title "Erfolg" --msgbox "Datei wurde dupliziert." 10 60
}

# Datei umbenennen
rename_file() {
    OLD_FILENAME=$(dialog --title "Datei umbenennen" --inputbox "Geben Sie den aktuellen Dateinamen ein:" 10 60 3>&1 1>&2 2>&3)
    NEW_FILENAME=$(dialog --title "Datei umbenennen" --inputbox "Geben Sie den neuen Dateinamen ein:" 10 60 3>&1 1>&2 2>&3)
    mv "$BASE_DIR/$OLD_FILENAME" "$BASE_DIR/$NEW_FILENAME"
    dialog --title "Erfolg" --msgbox "Datei wurde umbenannt." 10 60
}

# Alle Dateien archivieren
archive_files() {
    ARCHIVE_NAME="backup_$(date +%Y-%m-%d).tar.gz"
    tar -czf "$BASE_DIR/$ARCHIVE_NAME" "$BASE_DIR"
    dialog --title "Erfolg" --msgbox "Dateien wurden archiviert." 10 60
}

while true; do
    CHOICE=$(dialog --title "Dateimanager" --menu "Wählen Sie eine Aktion:" 15 60 5 \
    1 "Dateien auflisten" \
    2 "Statistiken anzeigen" \
    3 "Datei duplizieren" \
    4 "Datei umbenennen" \
    5 "Dateien archivieren" \
    3>&1 1>&2 2>&3)

    case $CHOICE in
        1)
            list_files
            ;;
        2)
            show_stats
            ;;
        3)
            duplicate_file
            ;;
        4)
            rename_file
            ;;
        5)
            archive_files
            ;;
        *)
            exit
            ;;
    esac
done
